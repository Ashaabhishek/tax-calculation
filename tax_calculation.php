<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main_div">
        <div class="header_div">
            <button type="button" id="logout"><a href="logout.php" tite="Logout"><b>LOGOUT<b></a></button> 
            <div id="first_letter" style="color: white; font-size: 45px; padding: 5px; float: right; margin-top: 20px; margin-right:5%; background-color: #666b6b; border-radius: 5px;"> 
            <?php echo substr(($_SESSION["username"]),0,1); ?> 
            </div>
        </div> 
        <div class="side_div"><br><br><br><br><br><br>
            <center>
            <a href="tax_calculation.php ">CALCULATE TAX</a><br><br>
            <a href="edit_profile.php">EDIT PROFILE</a>
            </center>
        </div>
        </div>
    <div class="container" align="center">
        <h2> TAX CALCULATION </h2>
        <form method="POST" action=" ">
            <label for="fullname"><b>Full Name</b></label>&nbsp;&nbsp;&nbsp;
            <input type="text" name="fullname" id="fullname" class="input" value= " <?php echo $_SESSION["username"];?>"><br><br>
            <label for="Age"><b>Age</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="age" id="age" class="input"><br>
            <span  id="age_error"></span><br><br>
            <label for="annual_income"><b>Annual income</b></label>&nbsp;
            <input type="text" name="salary" id="salary" class="input"><br>
            <span  id="salary_error"></span><br><br>
            <input type="submit" value="Submit" name="submit" id="submit">
        </form>
    </div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script> 
$(document).on("click", "#submit", function()
{
    var age = $("#age").val();
    var salary = $("#salary").val();
    is_success = true;
    if(age == '') 
    {
        $("#age").addClass('error_class');
        is_success = false;
    }
    if(salary == '') 
    {
        $("#salary").addClass('error_class');
        is_success = false;
    }
    if(age != '')
    {
        if(!($.isNumeric(age)))
        {
            $("#age_error").text("Your entered age is invalid.");
            $("#age_error").show();
            is_success = false;
        }
        else
        {
            $("#age_error").hide();
        }
    }
    if(salary != '')
    {
        if(!($.isNumeric(salary)))
        {
            $("#salary_error").text("Your entered salary is invalid.");
            $("#salary_error").show();
            is_success = false;
        }
    }
    if(!is_success) 
    {
        return false;
    } 
}); 
</script>
<?php
include "config.php";
if(isset($_POST['submit']))
{
    $age = mysqli_real_escape_string($con,$_POST['age']);
    $salary = mysqli_real_escape_string($con,$_POST['salary']);
    Global $total_tax, $total_tax1;
    $sql ="SELECT * FROM `admin_income` WHERE age<{$age} and age in (SELECT max(age) FROM `admin_income` WHERE age<{$age} order by `age` desc) order by `age` desc";
    $result = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($result))
    {
        $start_income = $row['start_incom'] ;
        $end_income = $row['end_incom'] ;
        $percentage = $row['percentage'] ;
        if($salary >= $start_income && $salary >=$end_income)
        {
            $diff = $end_income - $start_income;
            $tax = ($percentage/100)*$diff;
            $total_tax = $total_tax + $tax;
        }
        elseif($salary >= $start_income && $salary <= $end_income)
        {
            $diff1 = $salary - $start_income;
            $tax1 = ($percentage/100)*$diff1;
            $total_tax1 = $total_tax + $tax1;
        }
    }
if(isset($total_tax1))
{
    ?> <center> <?php echo "Your calculated yearly Tax is: ".$total_tax1; ?> </center> <?php 
}
else
{
    ?> <center> <?php echo "Tax is not applicable..."; ?> </center> <?php 
}       
}
?>
</body>
</html>
